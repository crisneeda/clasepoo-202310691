﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenU2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int C1_1, C1_2, C2_1, C2_2;
            C1_1 = Convert.ToInt32(txt1I1.Text);
            C1_2 = Convert.ToInt32(txt1I2.Text);
            C2_1 = Convert.ToInt32(txt2I1.Text);
            C2_2 = Convert.ToInt32(txt2I2.Text);

            CalcularMatriz objCalMat = new CalcularMatriz(C1_1, C1_2, C2_1, C2_2);

            int Determinate = objCalMat.GetRes();
            lblResultado.Text = Convert.ToString("|A| = "+Determinate);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
