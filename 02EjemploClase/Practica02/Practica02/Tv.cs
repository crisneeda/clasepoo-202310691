﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica02
{
    class Tv
    {
        private int tamano = 0;
        private int volumen = 0;
        private string color = "";
        private int brillo = 0;
        private int contraste = 0;
        private string marca = "";

        public void settamano(int tamano)
        {
            this.tamano = tamano;
        }
        
        public void setvolumen(int vol)
        {
            this.volumen = vol;
        }

        public void setcolor (string col)
        {
            this.color = col;
        }

        public void setbrillo (int brillo)
        {
            this.brillo = brillo;
        }

        public void setcontraste (int contras)
        {
            this.contraste = contras;
        }
        public void setmarca (string marc)
        {
            this.marca = marc;
        }

        public int gettamano()
        {
            return this.tamano;
        }
        public int getvolumen()
        {
            return this.volumen;
        }
        public string getcolor()
        {
            return this.color;
        }
        public int getbrillo()
        {
            return this.brillo;
        }
        public int getcontraste()
        {
            return this.contraste;
        }
        public string getmarca()
        {
            return this.marca;
        }
    }
}
