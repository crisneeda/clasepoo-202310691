﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnClase_Click(object sender, EventArgs e)
        {
            Tv tele1 = new Tv();

            tele1.settamano(45);
            int tamtv = tele1.gettamano();
            MessageBox.Show("El tamaño de la televisión 1 es de " + tamtv);

            tele1.setvolumen(32);
            int voltv = tele1.getvolumen();
            MessageBox.Show("El volumen de la televisión 1 es de " + voltv);

            tele1.setcolor("gris");
            string coltv = tele1.getcolor();
            MessageBox.Show("El color de la televisión 1 es de " + coltv);

            tele1.setbrillo(34);
            int britv = tele1.getbrillo();
            MessageBox.Show("El brillo de la televisión 1 es de " + britv);

            tele1.setcontraste(23);
            int contrtv = tele1.getcontraste();
            MessageBox.Show("El contraste de la televisión 1 es de " + contrtv);

            tele1.setmarca("Lg");
            string martv = tele1.getmarca();
            MessageBox.Show("La marca de la televisión 1 es " + martv);
        }
    }
}
