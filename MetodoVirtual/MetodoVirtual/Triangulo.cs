﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodoVirtual
{
    class Triangulo
    {
        public virtual void Area(double b, double h)
        {
            double R = (b * h) / 2;
            Console.WriteLine("Area Triangulo es: " + R);
        }
    }
}
