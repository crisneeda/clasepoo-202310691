﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodoVirtual
{
    class Cuadrado:Triangulo
    {
        public override void Area(double b, double h)
        {
            double R = b * h;
            Console.WriteLine("Area Cuadrado es: " + R);
        }
    }
}
