﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodoVirtual
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangulo objTrian = new Triangulo();
            objTrian.Area(10, 15);

            Cuadrado objCuad = new Cuadrado();
            objCuad.Area(10, 15);

            Console.ReadKey();
        }
    }
}
