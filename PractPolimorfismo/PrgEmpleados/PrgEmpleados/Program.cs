﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrgEmpleados
{
    class Program
    {
        static void Main(string[] args)
        {
            //For que sirve para volver a ingresar un valor
            for (int i = 0; i < 1; i++)
            {
                //Valor que sera usado para poder escojer que objeto instanciar
                Console.WriteLine("Ingresa a que empleado quieres calcular el salario");
                Console.WriteLine(" Conserje(0)\n Producción(1)\n Administrativo(2)\n Gerente(3)\n Dueño(4)");
                string r = Console.ReadLine();

                switch (r)
                {
                    case "0":
                        Console.WriteLine("Ingrese la cantidad de horas");
                        int hrs = Convert.ToInt32(Console.ReadLine());
                        Conserje objConserje = new Conserje();
                        int salario = objConserje.Calcular_Salario(hrs);
                        Console.WriteLine("Salario: " + salario);
                        break;
                    case "1":
                        Console.WriteLine("Ingrese la cantidad de horas");
                        hrs = Convert.ToInt32(Console.ReadLine());
                        Produccion objProduccion = new Produccion();
                        salario = objProduccion.Calcular_Salario(hrs);
                        Console.WriteLine("Salario: " + salario);
                        break;
                    case "2":
                        Console.WriteLine("Ingrese la cantidad de horas");
                        hrs = Convert.ToInt32(Console.ReadLine());
                        Administrativo objAdmin = new Administrativo();
                        salario = objAdmin.Calcular_Salario(hrs);
                        Console.WriteLine("Salario: " + salario);
                        break;
                    case "3":
                        Console.WriteLine("Ingrese la cantidad de horas");
                        hrs = Convert.ToInt32(Console.ReadLine());
                        Gerente objGerente = new Gerente();
                        salario = objGerente.Calcular_Salario(hrs);
                        Console.WriteLine("Salario: " + salario);
                        break;
                    case "4":
                        Console.WriteLine("Ingrese la cantidad de horas");
                        hrs = Convert.ToInt32(Console.ReadLine());
                        Dueño objDueño = new Dueño();
                        salario = objDueño.Calcular_Salario(hrs);
                        Console.WriteLine("Salario: " + salario);
                        break;
                }

                //Pregunta para tranformar el valor de i a -1, o terminar el for
                Console.WriteLine("¿Desea cerrar? si(1) no(0)");
                int close = Convert.ToInt32(Console.ReadLine());
                if (close == 1)
                {
                    Console.WriteLine("Presione cualquier tecla para cerrar");
                }
                else
                {
                    i = -1;
                }
            }
            Console.ReadKey();
        }
    }
}
