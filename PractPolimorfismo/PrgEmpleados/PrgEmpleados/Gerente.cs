﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrgEmpleados
{
    //Clase que hereda de la clase base y usa polimorfismo
    //Solo puse esta como ejemplo, ya que todas las demás son básicamente lo mismo
    class Gerente : Empleados
    {
        private int pago_geren = 120;

        public override int Calcular_Salario(int h)
        {
            return this.pago = h * pago_geren;
        }
    }
}