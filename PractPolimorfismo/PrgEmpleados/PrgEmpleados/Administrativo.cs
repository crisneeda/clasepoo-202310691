﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrgEmpleados
{
    class Administrativo:Empleados
    {
        private int pago_admin = 50;
        public override int Calcular_Salario(int h)
        {
            return this.pago = h * pago_admin;
        }
    }
}
