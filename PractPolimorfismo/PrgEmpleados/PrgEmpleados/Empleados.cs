﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrgEmpleados
{
    //Clase principal, la cual guarda el atributo pago y un metodo virtual
    class Empleados
    {
        protected int pago;
        public virtual int Calcular_Salario(int hrs)
        {
            return pago;
        }
    }
}
