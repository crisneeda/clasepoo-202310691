﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrgEmpleados
{
    class Produccion:Empleados
    {
        private int pago_prod = 35;

        public override int Calcular_Salario(int h)
        {
            return this.pago = h * pago_prod;
        }
    }
}
