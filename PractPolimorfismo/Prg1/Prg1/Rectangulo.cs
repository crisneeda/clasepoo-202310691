﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Rectangulo:Base
    {
        public override double Calcular_Area(double Largo, double Ancho)
        {
            return Area = Largo * Ancho;
        }
    }
}
