﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    //Clase hija de base que usa el metodo virtual
    // Nota: Hacen lo mismo las otras 2 solo que con cambios en los parametros y calculos
    class Circunferencia:Base
    {
        public override double Calcular_Area(double radio,double a)
        {
           return Area = Math.PI*(Math.Pow(radio,2));
        }
       
    }
}
