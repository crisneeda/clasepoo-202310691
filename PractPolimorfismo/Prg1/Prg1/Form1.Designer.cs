﻿
namespace Prg1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdbtn_Circun = new System.Windows.Forms.RadioButton();
            this.rdbtn_Rectan = new System.Windows.Forms.RadioButton();
            this.rdbtn_Trian = new System.Windows.Forms.RadioButton();
            this.btn_Calcula = new System.Windows.Forms.Button();
            this.btn_Cerrar = new System.Windows.Forms.Button();
            this.lbl_Area = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Largo = new System.Windows.Forms.TextBox();
            this.txt_Ancho = new System.Windows.Forms.TextBox();
            this.txt_Radio = new System.Windows.Forms.TextBox();
            this.txt_Altura = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rdbtn_Circun
            // 
            this.rdbtn_Circun.AutoSize = true;
            this.rdbtn_Circun.Location = new System.Drawing.Point(31, 61);
            this.rdbtn_Circun.Name = "rdbtn_Circun";
            this.rdbtn_Circun.Size = new System.Drawing.Size(93, 17);
            this.rdbtn_Circun.TabIndex = 0;
            this.rdbtn_Circun.TabStop = true;
            this.rdbtn_Circun.Text = "Circunferencia";
            this.rdbtn_Circun.UseVisualStyleBackColor = true;
            // 
            // rdbtn_Rectan
            // 
            this.rdbtn_Rectan.AutoSize = true;
            this.rdbtn_Rectan.Location = new System.Drawing.Point(31, 84);
            this.rdbtn_Rectan.Name = "rdbtn_Rectan";
            this.rdbtn_Rectan.Size = new System.Drawing.Size(80, 17);
            this.rdbtn_Rectan.TabIndex = 1;
            this.rdbtn_Rectan.TabStop = true;
            this.rdbtn_Rectan.Text = "Rectangulo";
            this.rdbtn_Rectan.UseVisualStyleBackColor = true;
            // 
            // rdbtn_Trian
            // 
            this.rdbtn_Trian.AutoSize = true;
            this.rdbtn_Trian.Location = new System.Drawing.Point(31, 107);
            this.rdbtn_Trian.Name = "rdbtn_Trian";
            this.rdbtn_Trian.Size = new System.Drawing.Size(69, 17);
            this.rdbtn_Trian.TabIndex = 2;
            this.rdbtn_Trian.TabStop = true;
            this.rdbtn_Trian.Text = "Triangulo";
            this.rdbtn_Trian.UseVisualStyleBackColor = true;
            // 
            // btn_Calcula
            // 
            this.btn_Calcula.Location = new System.Drawing.Point(202, 61);
            this.btn_Calcula.Name = "btn_Calcula";
            this.btn_Calcula.Size = new System.Drawing.Size(63, 23);
            this.btn_Calcula.TabIndex = 3;
            this.btn_Calcula.Text = "Calcula";
            this.btn_Calcula.UseVisualStyleBackColor = true;
            this.btn_Calcula.Click += new System.EventHandler(this.btn_Calcula_Click);
            // 
            // btn_Cerrar
            // 
            this.btn_Cerrar.Location = new System.Drawing.Point(202, 90);
            this.btn_Cerrar.Name = "btn_Cerrar";
            this.btn_Cerrar.Size = new System.Drawing.Size(63, 23);
            this.btn_Cerrar.TabIndex = 4;
            this.btn_Cerrar.Text = "Cerrar";
            this.btn_Cerrar.UseVisualStyleBackColor = true;
            this.btn_Cerrar.Click += new System.EventHandler(this.btn_Cerrar_Click);
            // 
            // lbl_Area
            // 
            this.lbl_Area.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_Area.Location = new System.Drawing.Point(171, 190);
            this.lbl_Area.Name = "lbl_Area";
            this.lbl_Area.Size = new System.Drawing.Size(126, 79);
            this.lbl_Area.TabIndex = 5;
            this.lbl_Area.Text = "Area";
            this.lbl_Area.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Largo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ancho:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Radio:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Altura:";
            // 
            // txt_Largo
            // 
            this.txt_Largo.Location = new System.Drawing.Point(70, 175);
            this.txt_Largo.Name = "txt_Largo";
            this.txt_Largo.Size = new System.Drawing.Size(96, 20);
            this.txt_Largo.TabIndex = 10;
            // 
            // txt_Ancho
            // 
            this.txt_Ancho.Location = new System.Drawing.Point(69, 201);
            this.txt_Ancho.Name = "txt_Ancho";
            this.txt_Ancho.Size = new System.Drawing.Size(96, 20);
            this.txt_Ancho.TabIndex = 11;
            // 
            // txt_Radio
            // 
            this.txt_Radio.Location = new System.Drawing.Point(69, 227);
            this.txt_Radio.Name = "txt_Radio";
            this.txt_Radio.Size = new System.Drawing.Size(96, 20);
            this.txt_Radio.TabIndex = 12;
            // 
            // txt_Altura
            // 
            this.txt_Altura.Location = new System.Drawing.Point(69, 253);
            this.txt_Altura.Name = "txt_Altura";
            this.txt_Altura.Size = new System.Drawing.Size(96, 20);
            this.txt_Altura.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Tipo de figura";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(67, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Datos";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 343);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txt_Altura);
            this.Controls.Add(this.txt_Radio);
            this.Controls.Add(this.txt_Ancho);
            this.Controls.Add(this.txt_Largo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_Area);
            this.Controls.Add(this.btn_Cerrar);
            this.Controls.Add(this.btn_Calcula);
            this.Controls.Add(this.rdbtn_Trian);
            this.Controls.Add(this.rdbtn_Rectan);
            this.Controls.Add(this.rdbtn_Circun);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdbtn_Circun;
        private System.Windows.Forms.RadioButton rdbtn_Rectan;
        private System.Windows.Forms.RadioButton rdbtn_Trian;
        private System.Windows.Forms.Button btn_Calcula;
        private System.Windows.Forms.Button btn_Cerrar;
        private System.Windows.Forms.Label lbl_Area;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_Largo;
        private System.Windows.Forms.TextBox txt_Ancho;
        private System.Windows.Forms.TextBox txt_Radio;
        private System.Windows.Forms.TextBox txt_Altura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

