﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prg1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Metodo del boton calcula
        private void btn_Calcula_Click(object sender, EventArgs e)
        {
            // if's que ayudan a saver que radio button esta selccionado, y así instanciar
            //La clase que se requiere
            if (rdbtn_Circun.Checked == true)
            {
                Circunferencia objCir = new Circunferencia();
                double radio = Convert.ToDouble(txt_Radio.Text);
                double Area = objCir.Calcular_Area(radio, 0);
                lbl_Area.Text = "A= " + Area;
            }

            if (rdbtn_Rectan.Checked == true)
            {
                Rectangulo objRectan = new Rectangulo();
                double ancho = Convert.ToDouble(txt_Ancho.Text);
                double largo = Convert.ToDouble(txt_Largo.Text);
                double Area = objRectan.Calcular_Area(ancho, largo);
                lbl_Area.Text = "A= " + Area;
            }

            if (rdbtn_Trian.Checked == true)
            {
                Triangulo objTriang = new Triangulo();
                double altura = Convert.ToDouble(txt_Altura.Text);
                double largo = Convert.ToDouble(txt_Largo.Text);
                double Area = objTriang.Calcular_Area(largo, altura);
                lbl_Area.Text = "A= " + Area;
            }
        }

        //Metodo del boton cerrar que al clickarlo, ejecuta el metodo Close de Forms
        private void btn_Cerrar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
