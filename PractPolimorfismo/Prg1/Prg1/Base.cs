﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    //Clase base que se usa para heredar a las otras 3
    class Base
    {
        //Atributo que guarda el area
        protected double Area;
        //Metodo virtual que se utiliza para el polimorfismo
        public virtual double Calcular_Area(double a, double b)
        {
            return Area;
        }
    }
}
