﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Triangulo:Base
    {
        public override double Calcular_Area(double Largo, double Altura)
        {
            return Area = Largo * ((Altura / 2));
        }
    }
}
