﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractClases02
{
    class Circunferencia
    {
        public double Radio = 0;

        public void area()
        {
            double Area = Math.PI * Math.Pow(this.Radio, 2);
            Console.WriteLine("es: " + Area);
        }
        public void perim()
        {
            double Per = (2 * Math.PI) * this.Radio;
            Console.WriteLine("es: " + Per);
        }
            
    }
}
