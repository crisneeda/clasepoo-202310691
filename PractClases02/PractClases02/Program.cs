﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractClases02
{
    class Program
    {
        static void Main(string[] args)
        {
            Circunferencia Moneda = new Circunferencia();
            Circunferencia Rueda = new Circunferencia();

            Rueda.Radio = 10;
            Moneda.Radio = 1;

            Console.Write("El área de la RUEDA ");
            Rueda.area();
            Console.Write("El área de la MONEDA ");
            Moneda.area();
            Console.WriteLine();
            Console.Write("El perímetro de la RUEDA ");
            Rueda.perim();
            Console.Write("El perímetro de la MONEDA ");
            Moneda.perim();

            Console.ReadKey();
            

        }
    }
}
