﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloBidimensional
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] Matriz = new int[2, 4];

            for (int i = 0; i < 2; i++)
            {

                for (int j = 0; j < 4; j++)
                {
                    Console.Write("Ingresa un valor de [" + i + "," + j + "]: ");
                    int valor = int.Parse(Console.ReadLine());
                    Matriz[i, j] = valor;
                }
            }

            Console.WriteLine("La matriz es: ");
            for (int i = 0; i < 2; i++)
            {

                for (int j = 0; j < 4; j++)
                {
                    Console.Write(Matriz[i, j]+"   ");
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
