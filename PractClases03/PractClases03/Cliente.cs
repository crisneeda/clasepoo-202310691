﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractClases03
{
    class Cliente
    {
        private string ApellidoMaterno;
        private string ApellidoPaterno;
        private int CP;
        private string Direccion;
        private int IDCliente;
        private string Nombre;
        private int Telefono;
        private int TelefonoCasa;
        private int TelefonoMovil;

        public void EliminaCliente()
        {
                this.ApellidoMaterno = "";
                this.ApellidoPaterno = "";
                this.CP = 0;
                this.Direccion = "";
                this.IDCliente = 0;
                this.Nombre = "";
                this.Telefono = 0;
                this.TelefonoCasa = 0;
                this.TelefonoMovil = 0;
        }
        public void InsertaCliente()
        {
            Console.Write("Apellido Materno: ");
            String ApellM = Console.ReadLine();
            this.ApellidoMaterno = ApellM;
            Console.Write("Apellido Paterno: ");
            String ApellP = Console.ReadLine();
            this.ApellidoPaterno = ApellP;
            Console.Write("Código Postal: ");
            int CodPos = int.Parse(Console.ReadLine());
            this.CP = CodPos;
            Console.Write("Dirección: ");
            String Dir = Console.ReadLine();
            this.Direccion = Dir;
            Console.Write("ID del cliente: ");
            int IdClien = int.Parse(Console.ReadLine());
            this.IDCliente = IdClien;
            Console.Write("Nombre: ");
            String Nom = Console.ReadLine();
            this.Nombre = Nom;
            Console.Write("Telefóno: ");
            int Tel = int.Parse(Console.ReadLine());
            this.Telefono = Tel;
            Console.Write("Telefóno Casa: ");
            int TelCasa = int.Parse(Console.ReadLine());
            this.TelefonoCasa = TelCasa;
            Console.Write("Telefóno Móvil: ");
            int TelMovil = int.Parse(Console.ReadLine());
            this.TelefonoMovil = TelMovil;
        }
        public void MostrarCliente()
        {
            Console.WriteLine("Apellido Materno: "+this.ApellidoMaterno);
            Console.WriteLine("Apellido Paterno: "+this.ApellidoPaterno);
            Console.WriteLine("Código Postal: "+this.CP);
            Console.WriteLine("Dirección: "+this.Direccion);
            Console.WriteLine("ID del cliente: "+this.IDCliente);
            Console.WriteLine("Nombre: "+this.Nombre);
            Console.WriteLine("Telefóno: "+this.Telefono);
            Console.WriteLine("Telefóno Casa: "+this.TelefonoCasa);
            Console.WriteLine("Telefóno Móvil: "+this.TelefonoMovil);
        }
    }
}
