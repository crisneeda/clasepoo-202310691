﻿
namespace Prog3
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.rdbtn_OutRange = new System.Windows.Forms.RadioButton();
            this.rdbtn_Zero = new System.Windows.Forms.RadioButton();
            this.rdbtn_Format = new System.Windows.Forms.RadioButton();
            this.rdbtn_Mismatch = new System.Windows.Forms.RadioButton();
            this.rdbtn_StackOver = new System.Windows.Forms.RadioButton();
            this.rdbtn_Cast = new System.Windows.Forms.RadioButton();
            this.rdbtn_Overflow = new System.Windows.Forms.RadioButton();
            this.btn_Iniciar = new System.Windows.Forms.Button();
            this.txt_Dato = new System.Windows.Forms.TextBox();
            this.lbl_a = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rdbtn_OutRange
            // 
            this.rdbtn_OutRange.AutoSize = true;
            this.rdbtn_OutRange.Location = new System.Drawing.Point(29, 36);
            this.rdbtn_OutRange.Name = "rdbtn_OutRange";
            this.rdbtn_OutRange.Size = new System.Drawing.Size(158, 17);
            this.rdbtn_OutRange.TabIndex = 0;
            this.rdbtn_OutRange.TabStop = true;
            this.rdbtn_OutRange.Text = "IndexOutOfRangeException";
            this.rdbtn_OutRange.UseVisualStyleBackColor = true;
            // 
            // rdbtn_Zero
            // 
            this.rdbtn_Zero.AutoSize = true;
            this.rdbtn_Zero.Location = new System.Drawing.Point(29, 59);
            this.rdbtn_Zero.Name = "rdbtn_Zero";
            this.rdbtn_Zero.Size = new System.Drawing.Size(136, 17);
            this.rdbtn_Zero.TabIndex = 1;
            this.rdbtn_Zero.TabStop = true;
            this.rdbtn_Zero.Text = "DivideByZeroException";
            this.rdbtn_Zero.UseVisualStyleBackColor = true;
            // 
            // rdbtn_Format
            // 
            this.rdbtn_Format.AutoSize = true;
            this.rdbtn_Format.Location = new System.Drawing.Point(29, 82);
            this.rdbtn_Format.Name = "rdbtn_Format";
            this.rdbtn_Format.Size = new System.Drawing.Size(104, 17);
            this.rdbtn_Format.TabIndex = 2;
            this.rdbtn_Format.TabStop = true;
            this.rdbtn_Format.Text = "FormatException";
            this.rdbtn_Format.UseVisualStyleBackColor = true;
            this.rdbtn_Format.CheckedChanged += new System.EventHandler(this.rdbtn_Format_CheckedChanged);
            // 
            // rdbtn_Mismatch
            // 
            this.rdbtn_Mismatch.AutoSize = true;
            this.rdbtn_Mismatch.Location = new System.Drawing.Point(29, 151);
            this.rdbtn_Mismatch.Name = "rdbtn_Mismatch";
            this.rdbtn_Mismatch.Size = new System.Drawing.Size(165, 17);
            this.rdbtn_Mismatch.TabIndex = 3;
            this.rdbtn_Mismatch.TabStop = true;
            this.rdbtn_Mismatch.Text = "ArrayTypeMismatchException";
            this.rdbtn_Mismatch.UseVisualStyleBackColor = true;
            // 
            // rdbtn_StackOver
            // 
            this.rdbtn_StackOver.AutoSize = true;
            this.rdbtn_StackOver.Location = new System.Drawing.Point(29, 105);
            this.rdbtn_StackOver.Name = "rdbtn_StackOver";
            this.rdbtn_StackOver.Size = new System.Drawing.Size(142, 17);
            this.rdbtn_StackOver.TabIndex = 4;
            this.rdbtn_StackOver.TabStop = true;
            this.rdbtn_StackOver.Text = "StackOverflowException";
            this.rdbtn_StackOver.UseVisualStyleBackColor = true;
            // 
            // rdbtn_Cast
            // 
            this.rdbtn_Cast.AutoSize = true;
            this.rdbtn_Cast.Location = new System.Drawing.Point(29, 128);
            this.rdbtn_Cast.Name = "rdbtn_Cast";
            this.rdbtn_Cast.Size = new System.Drawing.Size(124, 17);
            this.rdbtn_Cast.TabIndex = 5;
            this.rdbtn_Cast.TabStop = true;
            this.rdbtn_Cast.Text = "InvalidCastException";
            this.rdbtn_Cast.UseVisualStyleBackColor = true;
            // 
            // rdbtn_Overflow
            // 
            this.rdbtn_Overflow.AutoSize = true;
            this.rdbtn_Overflow.Location = new System.Drawing.Point(29, 174);
            this.rdbtn_Overflow.Name = "rdbtn_Overflow";
            this.rdbtn_Overflow.Size = new System.Drawing.Size(114, 17);
            this.rdbtn_Overflow.TabIndex = 6;
            this.rdbtn_Overflow.TabStop = true;
            this.rdbtn_Overflow.Text = "OverflowException";
            this.rdbtn_Overflow.UseVisualStyleBackColor = true;
            this.rdbtn_Overflow.CheckedChanged += new System.EventHandler(this.rdbtn_Overflow_CheckedChanged);
            // 
            // btn_Iniciar
            // 
            this.btn_Iniciar.Location = new System.Drawing.Point(225, 105);
            this.btn_Iniciar.Name = "btn_Iniciar";
            this.btn_Iniciar.Size = new System.Drawing.Size(75, 23);
            this.btn_Iniciar.TabIndex = 7;
            this.btn_Iniciar.Text = "Iniciar";
            this.btn_Iniciar.UseVisualStyleBackColor = true;
            this.btn_Iniciar.Click += new System.EventHandler(this.btn_Iniciar_Click);
            // 
            // txt_Dato
            // 
            this.txt_Dato.Location = new System.Drawing.Point(236, 79);
            this.txt_Dato.Name = "txt_Dato";
            this.txt_Dato.Size = new System.Drawing.Size(55, 20);
            this.txt_Dato.TabIndex = 8;
            // 
            // lbl_a
            // 
            this.lbl_a.AutoSize = true;
            this.lbl_a.Location = new System.Drawing.Point(222, 63);
            this.lbl_a.Name = "lbl_a";
            this.lbl_a.Size = new System.Drawing.Size(0, 13);
            this.lbl_a.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(323, 237);
            this.Controls.Add(this.lbl_a);
            this.Controls.Add(this.txt_Dato);
            this.Controls.Add(this.btn_Iniciar);
            this.Controls.Add(this.rdbtn_Overflow);
            this.Controls.Add(this.rdbtn_Cast);
            this.Controls.Add(this.rdbtn_StackOver);
            this.Controls.Add(this.rdbtn_Mismatch);
            this.Controls.Add(this.rdbtn_Format);
            this.Controls.Add(this.rdbtn_Zero);
            this.Controls.Add(this.rdbtn_OutRange);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdbtn_OutRange;
        private System.Windows.Forms.RadioButton rdbtn_Zero;
        private System.Windows.Forms.RadioButton rdbtn_Format;
        private System.Windows.Forms.RadioButton rdbtn_Mismatch;
        private System.Windows.Forms.RadioButton rdbtn_StackOver;
        private System.Windows.Forms.RadioButton rdbtn_Cast;
        private System.Windows.Forms.RadioButton rdbtn_Overflow;
        private System.Windows.Forms.Button btn_Iniciar;
        private System.Windows.Forms.TextBox txt_Dato;
        private System.Windows.Forms.Label lbl_a;
    }
}

