﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Metodo del boton Iniciar
        private void btn_Iniciar_Click(object sender, EventArgs e)
        {
            //Los if´s son para saber si un radio buton esta seleccinado, y ejecutar un codigo de pendiendo de cual sea true
            if(rdbtn_OutRange.Checked==true)
            {
                int[] x = new int[5];
                try
                {
                    //El arreglo es muy pequeño para el tamaño del for
                    for (int i = 0; i < 10; i++)
                        x[i]++;
                }
                //Catch para la excepcion IndexOutRange
                catch(IndexOutOfRangeException Ex)
                {
                    MessageBox.Show("Error: " + Ex);
                }

            }

            if(rdbtn_Zero.Checked==true)
            {
                int x = 32;
                try
                {
                    //Hay una division entre cero
                    x = x / 0;
                }
                //Catch para la excepcion DivideByZero
                catch (DivideByZeroException Ex)
                {
                    MessageBox.Show("Error: " + Ex);
                }

            }

            if (rdbtn_Format.Checked==true)
            {
                int x;
                try
                {
                    //Aqui se pide al usuario que llene con una cadena, para que se genere el error
                    x = Convert.ToInt32(lbl_a.Text);
                }
                ////Catch para la excepcion Format
                catch (FormatException Ex)
                {
                    MessageBox.Show("Error: " + Ex);
                }

            }

            if (rdbtn_StackOver.Checked == true)
            {
                //Se llama a un metodo que se llama a si mismo
                //El try & catch estan en ese metodo
                StackOverflow();

            }

            if (rdbtn_Cast.Checked==true)
            {
                object x = new object();
                int y;
                try
                {
                    //Se trata de convertir un tipo de dato a uno que no se puede
                    y = Convert.ToInt32(x);
                }
                //Catch para la excepcion InvalidCast
                catch (InvalidCastException Ex)
                {  
                    MessageBox.Show("Error: " + Ex);
                }

            }

            if (rdbtn_Mismatch.Checked==true)
            {
                string[] x = new string[3];
                object[] y = x;
                try
                {
                    //Se le asigna un valor a un arreglo que no es del mismo tipo
                    y[1] = 4;
                }
                //Catch para la excepcion ArrayTypeMismatch
                catch (ArrayTypeMismatchException Ex)
                {
                    MessageBox.Show("Error: " + Ex);
                }
            }

            if (rdbtn_Overflow.Checked==true)
            {
                int x;
                try
                {
                    //Se le pide al usuario un numero muy grande para que la variable se desborde
                    x = int.Parse(txt_Dato.Text);
                }
                //Catch para la excepcion Overflow
                catch (OverflowException Ex)
                {
                    MessageBox.Show("Error: " + Ex);
                }
            }
        }
        //Metodo del radio button Format
        private void rdbtn_Format_CheckedChanged(object sender, EventArgs e)
        {
            //La label ahora tiene valor
            lbl_a.Text = ("Ingresa texto");
            //txt_Dato se vuelve true
            txt_Dato.Enabled = true;
            //si txt_Format es false, txt_Dato se vuelve false y la label se vacia 
            if(rdbtn_Format.Checked==false)
            {
                txt_Dato.Enabled = false;
                lbl_a.Text = "";
            }
        }
        //Metodo de Form1 
        private void Form1_Load(object sender, EventArgs e)
        {
            //Al cargarse la Form1 txt_Dato se vulve false
            txt_Dato.Enabled = false;
        }
        //Clase para la excepcion StackOverflow
        private void StackOverflow()
        {
            try
            {
                StackOverflow();
            }
            catch (StackOverflowException Ex)
            {
                MessageBox.Show("Error: " + Ex);
            }

        }
        //Metodo de rdctn_Overflow, que hace exactamente lo mismo que el metodo de rdbtn_Format pero consigo
        private void rdbtn_Overflow_CheckedChanged(object sender, EventArgs e)
        {
            lbl_a.Text = ("Ingresa un numero");
            txt_Dato.Enabled = true;
            if (rdbtn_Overflow.Checked == false)
            {
                txt_Dato.Enabled = false;
                lbl_a.Text = "";
            }
        }
    }
}
