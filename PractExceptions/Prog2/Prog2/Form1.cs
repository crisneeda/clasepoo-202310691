﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prog2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Metodo del boton Start, que al hacer click ejecuta un codigo
        private void btn_Start_Click(object sender, EventArgs e)
        {
            //if que sirve para saber si un radio buton esta seleccionado 
            if(rdbtn_Tam.Checked==true)
            {
                int[] array = new int[10];
                //Try para ejcutar codigo con posible error
                try
                {
                    //For que es mas grande que el arreglo
                    for (int i = 0; i < 15; i++)
                        array[i]++;
                }
                //Catch para saber que error es, en este caso es IndexOutOfRange y variable que se rellena con error
                //Ejecuta un codigo que esta dentro
                catch(IndexOutOfRangeException Range)
                {
                    MessageBox.Show("Error: " + Range);
                }
                //finally que termina de ejecutar el error y devuelve un Messagebox
                finally
                {
                    MessageBox.Show("El arreglo es muy pequeño");
                }
            }
            //if que sirve para saber si un radio buton esta seleccionado 
            if (rdbtn_Cero.Checked==true)
            {
                int x=132;
                //Try para ejcutar codigo con posible error

                try
                {
                    x = x / 0;
                }
                //Catch para saber que error es, en este caso es DivideByZero y variable que se rellena con error
                //Ejecuta un codigo que esta dentro
                catch (DivideByZeroException Zero)
                {
                    MessageBox.Show("Error: " + Zero);
                }
                //finally que termina de ejecutar el error y devuelve un Messagebox
                finally
                {
                    MessageBox.Show(x + " / " + " 0 = Inifinito");
                }
            }
        }

        private void rdbtn_Tam_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void rdbtn_Cero_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
