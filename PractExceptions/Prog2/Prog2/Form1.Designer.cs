﻿
namespace Prog2
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Start = new System.Windows.Forms.Button();
            this.rdbtn_Tam = new System.Windows.Forms.RadioButton();
            this.rdbtn_Cero = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(84, 134);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(75, 23);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Iniciar";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // rdbtn_Tam
            // 
            this.rdbtn_Tam.AutoSize = true;
            this.rdbtn_Tam.Location = new System.Drawing.Point(52, 52);
            this.rdbtn_Tam.Name = "rdbtn_Tam";
            this.rdbtn_Tam.Size = new System.Drawing.Size(135, 17);
            this.rdbtn_Tam.TabIndex = 1;
            this.rdbtn_Tam.TabStop = true;
            this.rdbtn_Tam.Text = "Error tamaño de arreglo";
            this.rdbtn_Tam.UseVisualStyleBackColor = true;
            this.rdbtn_Tam.CheckedChanged += new System.EventHandler(this.rdbtn_Tam_CheckedChanged);
            // 
            // rdbtn_Cero
            // 
            this.rdbtn_Cero.AutoSize = true;
            this.rdbtn_Cero.Location = new System.Drawing.Point(52, 75);
            this.rdbtn_Cero.Name = "rdbtn_Cero";
            this.rdbtn_Cero.Size = new System.Drawing.Size(119, 17);
            this.rdbtn_Cero.TabIndex = 2;
            this.rdbtn_Cero.TabStop = true;
            this.rdbtn_Cero.Text = "Error dividir por cero";
            this.rdbtn_Cero.UseVisualStyleBackColor = true;
            this.rdbtn_Cero.CheckedChanged += new System.EventHandler(this.rdbtn_Cero_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(244, 200);
            this.Controls.Add(this.rdbtn_Cero);
            this.Controls.Add(this.rdbtn_Tam);
            this.Controls.Add(this.btn_Start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.RadioButton rdbtn_Tam;
        private System.Windows.Forms.RadioButton rdbtn_Cero;
    }
}

