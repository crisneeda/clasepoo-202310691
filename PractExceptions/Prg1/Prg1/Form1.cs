﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prg1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Metodo del boton Start, que al clickarse ejecuta el siguiente codigo
        private void btn_Start_Click(object sender, EventArgs e)
        {
            //if para poder ver si el checkbox Tamaño esta ejecutandose, si es así corre un codigo
            if (chbox_Tamaño.Checked == true)
            {
                int[] array = new int[0];
                //Try que sirve para probar si en la linea de adentro hay una Excepcion
                try
                {
                    array[1] = Convert.ToInt32(txt_Dato.Text);
                }
                //catch que al ejecutarse una IndexOutOfRangeException, le asigna valores a la variable y
                //ejecuta la linea de codigo de dentro (MessageBox.Show)
                catch (IndexOutOfRangeException Range)
                {
                    MessageBox.Show("Error  " + Range);
                }
            }
            //if para poder ver si el checkbox Tipo esta ejecutandose, si es así corre un codigo
            if (chbox_Tipo.Checked==true)
            {
                int x;
                //Try que sirve para probar si en la linea de adentro hay una Excepcion
                try
                {
                    x = Convert.ToInt32(txt_Dato.Text);
                }
                //catch que al ejecutarse una Format, le asigna valores a la variable y
                //ejecuta la linea de codigo de dentro (MessageBox.Show)
                catch (FormatException Format)
                {
                    MessageBox.Show("Error  " + Format);
                }
            }
            
        }
        //Metodo del checkbox Tipo
        private void chbox_Tipo_CheckedChanged(object sender, EventArgs e)
        {
            //El chbox Tamaño se le asigna false
            chbox_Tamaño.Enabled = false;
            //Si el chbox Tipo es false, el chbox Tamaño se pone en true
            if (chbox_Tipo.Checked == false)
            {
                chbox_Tamaño.Enabled = true;
            }
        }
        //Metodo del checkbox Tamaño
        private void chbox_Tamaño_CheckedChanged(object sender, EventArgs e)
        {
            //Lo mismo que el metodo anterior pero a la inversa.
            chbox_Tipo.Enabled = false;
            if(chbox_Tamaño.Checked==false)
            {
                chbox_Tipo.Enabled = true;
            }
        }
    }
}
