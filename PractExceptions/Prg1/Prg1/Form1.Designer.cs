﻿
namespace Prg1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Start = new System.Windows.Forms.Button();
            this.chbox_Tipo = new System.Windows.Forms.CheckBox();
            this.chbox_Tamaño = new System.Windows.Forms.CheckBox();
            this.txt_Dato = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(191, 77);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(75, 23);
            this.btn_Start.TabIndex = 0;
            this.btn_Start.Text = "Iniciar";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // chbox_Tipo
            // 
            this.chbox_Tipo.AutoSize = true;
            this.chbox_Tipo.Location = new System.Drawing.Point(40, 31);
            this.chbox_Tipo.Name = "chbox_Tipo";
            this.chbox_Tipo.Size = new System.Drawing.Size(107, 17);
            this.chbox_Tipo.TabIndex = 1;
            this.chbox_Tipo.Text = "Error tipo de dato";
            this.chbox_Tipo.UseVisualStyleBackColor = true;
            this.chbox_Tipo.CheckedChanged += new System.EventHandler(this.chbox_Tipo_CheckedChanged);
            // 
            // chbox_Tamaño
            // 
            this.chbox_Tamaño.AutoSize = true;
            this.chbox_Tamaño.Location = new System.Drawing.Point(40, 54);
            this.chbox_Tamaño.Name = "chbox_Tamaño";
            this.chbox_Tamaño.Size = new System.Drawing.Size(121, 17);
            this.chbox_Tamaño.TabIndex = 2;
            this.chbox_Tamaño.Text = "Error tamaño arreglo";
            this.chbox_Tamaño.UseVisualStyleBackColor = true;
            this.chbox_Tamaño.CheckedChanged += new System.EventHandler(this.chbox_Tamaño_CheckedChanged);
            // 
            // txt_Dato
            // 
            this.txt_Dato.Location = new System.Drawing.Point(40, 116);
            this.txt_Dato.Name = "txt_Dato";
            this.txt_Dato.Size = new System.Drawing.Size(100, 20);
            this.txt_Dato.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 187);
            this.Controls.Add(this.txt_Dato);
            this.Controls.Add(this.chbox_Tamaño);
            this.Controls.Add(this.chbox_Tipo);
            this.Controls.Add(this.btn_Start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.CheckBox chbox_Tipo;
        private System.Windows.Forms.CheckBox chbox_Tamaño;
        private System.Windows.Forms.TextBox txt_Dato;
    }
}

