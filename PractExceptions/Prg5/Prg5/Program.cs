﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg5
{
    class Program
    {
        //Main en el que se piden valores para instanciar la clase "Cita"
        static void Main(string[] args)
        {
            Console.Write("Ingresa dia (incio en minúscula): ");
            string d = Console.ReadLine();
            Console.Write("Ingresa hora: ");
            int h = int.Parse(Console.ReadLine());
            Console.Write("Ingresa minuto: ");
            int m = int.Parse(Console.ReadLine());
            Console.Write("Ingresa descripción: ");
            string des = Console.ReadLine();

            Cita obCita = new Cita(d, h, m, des);

            Console.ReadKey();
        }
    }
}
