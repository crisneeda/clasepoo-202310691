﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg5
{
    //Clase cito con cuatro metodos privados, 2 int y 2 string
    class Cita
    {
        private string Dia;
        private int Hora;
        private int Minuto;
        private string Descripcion;
        //Constructor con 4 parametros
        public Cita(string dia,int hr,int min,string des)
        {
            //Switch para saber si dia es valido
            switch (dia)
            {
                case "lunes":
                    break;
                    Dia = dia;
                case "martes":
                    Dia = dia;
                    break;
                case "miercoles":
                    Dia = dia;
                    break;
                case "jueves":
                    Dia = dia;
                    break;
                case "viernes":
                    Dia = dia;
                    break;
                case "sabado":
                    Dia = dia;
                    break;
                case "domingo":
                    Dia = dia;
                    break;
                default:
                    //Default para generar una excepcion con throw
                    throw new ArgumentException("Dia invalido");
                    //Try para usar un codigo con un posible error
                    try
                    {
                        Dia = dia;
                    }
                    //Catch para la excepcion creada
                    catch(ArgumentException ex)
                    {
                        Console.WriteLine("Error: " + ex);
                    }
                    break;
            }

            //Los if's hacen lo mismo que el deafult del switch
            if(hr>=0 && hr<24)
                Hora = hr;
            else
            {
                throw new ArgumentException("Hora invalida (0 a 23)");
                try
                {
                    Hora = hr;
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("Error: " + ex);
                }
            }
               
            if (min >= 0 && min < 60)
                Minuto = min;
            else
            {
                throw new ArgumentException("Minuto invalid0 (0 a 59)");
                try
                {
                    Minuto = min;
                }
                catch (ArgumentException ex)
                {
                    Console.WriteLine("Error: " + ex);
                }
            }
            //Al atributo Descripcion se le da un valor
            Descripcion = des;
            //Console.Writline 
            Console.WriteLine("Cita: " + Dia + "a las " + Hora + ":" + Minuto+"\n Descripción: "+Descripcion);
        }
        
    }
}
