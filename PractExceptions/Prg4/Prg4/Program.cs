﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se le pide al usuario ingresar valores para poder realizar la operacion
            int a, b, c = 0;
            Console.WriteLine("Leer a y b");
            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            //Try para un codigo que puede contener un error
            try
            {
                //Se instancia la clase Ejemplo y se le nombra a su objeto "e"
                Ejemplo e = new Ejemplo();
                //Se llama al metodo "CalculaDivision" del objeto "e" y el return lo guarda en c
                c = e.CalculaDivision(a, b);
            }
            //Catch para detectar la excepcion previamente generada
            catch(Exception x)
            {
                Console.WriteLine(x.Message);
            }
            //Finally que ejecuta el codigo que contiene
            finally
            {
                Console.WriteLine(a + "/" + b + "=" + c);
            }

            Console.ReadKey();
        }
    }

    //Clase ejemplo
    class Ejemplo
    {
        //Metodo CalculaDivision pulic de tipo int, con dos parametros
        public int CalculaDivision(int numerador, int denominador)
        {
            //If para determinar si el parametro denominador es cero, si lo es se crea una excepcion con Throw
            //Si no retorna la division de los dos parametros
            if (denominador == 0)
                throw new Exception("El denominador NO debe ser cero");
            else
                return (numerador / denominador);
        }
    }
}
