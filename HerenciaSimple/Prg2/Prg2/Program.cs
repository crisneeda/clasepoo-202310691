﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instaciación de la clase Accion_realizada
            Accion_realizada objAccion = new Accion_realizada();
            //Instanciacion del objAccion y su metodo guardar
            objAccion.guardar();

            for (int i = 0; i < 1; i++)
            {
                //Pregunta que sirve para usar el switch
                Console.WriteLine("¿Qué acción desea realizar? listar  actualizar");
                string res = Console.ReadLine();
                switch (res)
                {
                    case "listar":
                        objAccion.listar();
                        break;
                    case "actualizar":
                        objAccion.actualizar();
                        break;
                }

                //Pregunta para tranformar el valor de i a -1, o terminar el for
                Console.WriteLine("¿Desea cerrar? si(1) no(0)");
                int close = Convert.ToInt32(Console.ReadLine());
                if (close == 1)
                {
                    Console.WriteLine("Presione cualquier tecla para cerrar");
                }
                else
                {
                    i = -1;
                }
            }

            Console.ReadKey();
        }
    }
}
