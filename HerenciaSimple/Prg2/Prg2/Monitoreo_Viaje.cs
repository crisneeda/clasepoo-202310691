﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2
{
    class Monitoreo_Viaje
    {
        //En esta clase se declaran todos los atributos a heredar
        protected int nro_monitoreo;
        protected string diagnostico_pac;
        protected int hora;
        protected int pulso;
        protected int presion;
        protected int lat_fetales;
        protected int nro_contraccion;
        protected int hem_cantidad;
        protected int convulciones_hora;
        protected bool paro_card;
        protected string otros;
    }
}
