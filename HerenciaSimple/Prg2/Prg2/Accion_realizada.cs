﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2
{
    class Accion_realizada : Monitoreo_Viaje
    {
        //Atributos propios de esta clase
        private int cod_accion;
        private string descripcion;
        private int tipo;

        //Metódo listar que escribe todos los valores
        public void listar()
        {
            Console.WriteLine("Número monitoreo: " + nro_monitoreo);
            Console.WriteLine("Diagnóstico: " + diagnostico_pac);
            Console.WriteLine("Hora: " + hora);
            Console.WriteLine("Pulso: " + pulso);
            Console.WriteLine("Presión: " + presion);
            Console.WriteLine("Latidos fetales: " + lat_fetales);
            Console.WriteLine("Número de contracciónes: " + nro_contraccion);
            Console.WriteLine("Cantidad de hemorragia: " + hem_cantidad);
            Console.WriteLine("Convulciones por hora: " + convulciones_hora);
            Console.WriteLine("Paro cardiaco: " + paro_card);
            Console.WriteLine("Otros: " + otros);
            Console.WriteLine("Código de acción: " + cod_accion);
            Console.WriteLine("Descripción: " + descripcion);
            Console.WriteLine("Tipo: " + tipo);

        }

        //Metodo que actualiza los atributos, y como guardar hace lo mismo, reciclo codigo llamando al metodo
        public void actualizar()
        {
            Console.WriteLine("Ingresa datos a acturalizar");
            guardar();
        }

        //Metodo que guarda por primera vez los atributos
        public void guardar() 
        { 
            Console.WriteLine("Ingresa datos a guardar");
            Console.Write("Número monitoreo: ");
            nro_monitoreo = int.Parse(Console.ReadLine());
            Console.Write("Diagnóstico: ");
            diagnostico_pac = Console.ReadLine();
            Console.Write("Hora: ");
            hora = int.Parse(Console.ReadLine());
            Console.Write("Pulso: ");
            pulso = int.Parse(Console.ReadLine());
            Console.Write("Presión: ");
            presion = int.Parse(Console.ReadLine());
            Console.Write("Latidos fetales: ");
            lat_fetales = int.Parse(Console.ReadLine());
            Console.Write("Número de contracciónes: ");
            nro_contraccion = int.Parse(Console.ReadLine());
            Console.Write("Cantidad de hemorragia: ");
            hem_cantidad = int.Parse(Console.ReadLine());
            Console.Write("Convulciones por hora: ");
            convulciones_hora = int.Parse(Console.ReadLine());
            Console.Write("Paro cardiaco (True  False): ");
            paro_card = Convert.ToBoolean(Console.ReadLine());
            Console.Write("Otros: ");
            otros = Console.ReadLine();
            Console.Write("Código de acción: ");
            cod_accion = int.Parse(Console.ReadLine());
            Console.Write("Descripción: ");
            descripcion = Console.ReadLine();
            Console.Write("Tipo: ");
            tipo = int.Parse(Console.ReadLine());
        }
    }
}
