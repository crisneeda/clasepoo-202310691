﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Flight:Airline
    {
        //Atributos de la clase
        protected string Destination;
        protected string Departure;
        protected string Flight_Date;
        protected string Flight_Time;
        protected int Flight_Price;
        protected int Flight_Number;

        //Constructor con herencia
        public Flight(string Des, string Dep, string Date, string Time, int Price, int Num, bool Dis):base(Dis)
        {
            Reservation_Availablity = Dis;
            Destination = Des;
            Departure = Dep;
            Flight_Date = Date;
            Flight_Time = Time;
            Flight_Price = Price;
            Flight_Number = Num;
        }

        //Metodo que imprime los atributos
        public void Imprimir()
        {
            Console.WriteLine("Destination: " + Destination);
            Console.WriteLine("Departure: " + Departure);
            Console.WriteLine("Flight Date: " + Flight_Date);
            Console.WriteLine("Flight Time: " + Flight_Time);
            Console.WriteLine("Flight Price: " + Flight_Price);
            Console.WriteLine("Flight Number: " + Flight_Number);
        }
    }
}
