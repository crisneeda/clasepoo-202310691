﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main, en el que se pide al usuario responder y dependiendo de resulatado, intancia la clase res
            //con un true o un false en el constructor
            Console.WriteLine("¿Hay vuelos disponibles? Si No");
            string r = Console.ReadLine();
            bool res;
            switch(r)
            {
                case ("Si"):
                    res = true;
                    break;
                default:
                    res = false;
                    break;
            }

            Airline objAirline = new Airline(res);
            //Inicializacion del metod Opcion
            objAirline.Opcion();
            Console.ReadKey();
        }
    }
}
