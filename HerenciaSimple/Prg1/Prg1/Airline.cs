﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Airline
    {
        //Se declara el atributo bool
        protected bool Reservation_Availablity;

        //Constructor que asigna valor al atributo reservation
        public Airline(bool Dis)
        {
            Reservation_Availablity = Dis;
        }

        //Metodo que dependiendo de el valor de reservation, instancia un objeto o no
        public void Opcion()
        {
            if(Reservation_Availablity==true)
            {
                Console.WriteLine("Ingresa Destination");
                string Des = Console.ReadLine();
                Console.WriteLine("Ingresa Departure");
                string Dep = Console.ReadLine();
                Console.WriteLine("Ingresa Flight Date");
                string Date = Console.ReadLine();
                Console.WriteLine("Ingresa Flight Time");
                string Tim = Console.ReadLine();
                Console.WriteLine("Ingresa Flight Price");
                int Price = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Ingresa Flight Number");
                int Num = Convert.ToInt32(Console.ReadLine());

                Flight objFlight = new Flight(Des, Dep, Date, Tim, Price, Num, this.Reservation_Availablity);
                objFlight.Imprimir();
            }
            else
            {
                Console.WriteLine("Presiona cualquier tecla para cerrar");
            }
        }
    }
}
