﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main donde se instancian las clases como objetos
            Punto2D p2d = new Punto2D();
            Punto3D p3d = new Punto3D();
            //Se inicializan los meyodos de los 2 objetos,con algunos con herencia
            Console.WriteLine(p2d.X = 100);
            Console.WriteLine(p2d.Y = 200);
            Console.WriteLine(p3d.X = 150);
            Console.WriteLine(p3d.Y = 250);
            Console.WriteLine(p3d.Z = 350);
            Console.ReadLine();
        }
    }
}
