﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Act1
{
    class Operacion
    {
        private int n;
        public void ImprimePares(int setN)
        {
            n = setN;
            int [] array = new int[n];
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("Ingresa el valor del elemento [" + i + "]: ");
                array[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i]%2==0)
                {
                    Console.WriteLine("El numero " + array[i] + " es par");
                }
                else
                {
                    Console.WriteLine("El numero " + array[i] + " no es par");
                }
            }
            

        }

        public void SumaArreglo(int setN)
        {
            n = setN;
            int[] array = new int[n];
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write("Ingresa el valor del elemento [" + i + "]: ");
                array[i] = Convert.ToInt32(Console.ReadLine());
            }

            int suma = 0;
            for (int i = 0; i < array.Length; i++)
            {
                suma +=array[i];
            }
            Console.WriteLine("La suma de todos los elementos del arreglo es: " + suma);
        }
        
    }
}
