﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Act1
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacion objOper = new Operacion();
            
            Console.WriteLine("¿Qué quieres hacer? Imprimir pares[pares] | Sumar arreglo[sumar]");
            string res = Console.ReadLine();
            int tam;
            switch (res)
            {
                case "pares":
                    Console.WriteLine("Ingresa el tamaño del arreglo");
                    tam = int.Parse(Console.ReadLine());
                    objOper.ImprimePares(tam);
                    break;

                case "sumar":
                    Console.WriteLine("Ingresa el tamaño del arreglo");
                    tam = int.Parse(Console.ReadLine());
                    objOper.SumaArreglo(tam);
                    break;

            }
            Console.ReadKey();
        }
    }
}
