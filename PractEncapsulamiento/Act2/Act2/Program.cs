﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Act2
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacion objOper = new Operacion();

            Console.WriteLine("Ingresa el tamaño de N de la matriz");
            int f = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa el tamaño de M de la matriz");
            int c = int.Parse(Console.ReadLine());

            objOper.LeerMatriz(f, c);
            objOper.ImprimeMatriz();

            Console.ReadKey();
        }
    }
}
