﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Act2
{
    class Operacion
    {
        private int n;
        private int m;
        private int[,] matriz;

        public void ImprimeMatriz()
        {
            Console.WriteLine("La matriz es: ");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write(matriz[i, j]+"  "); 
                }
                Console.WriteLine();
            }

        }

        public void LeerMatriz(int setN,int setM)
        {
            n = setN;
            m = setM;
            matriz = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.Write("Ingresa un valor para [" + i + "," + j + "]:");
                    matriz[i, j] = int.Parse(Console.ReadLine());
                }
            }

        }
    }
}
