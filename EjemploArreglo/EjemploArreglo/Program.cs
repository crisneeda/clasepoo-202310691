﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploArreglo
{
    class Program
    {
        static void Main(string[] args)
        {
            int acum = 0;
            int[] CatalogoNum = new int[10];

            for (int i = 0; i < CatalogoNum.Length; i++)
            {
                Console.Write("Ingresa un número:");
                int n =int.Parse(Console.ReadLine());
                CatalogoNum[i] = n;
            }

            Console.WriteLine();
            foreach (int var in CatalogoNum) /* Foreach Imprime arreglos */
            {
                Console.WriteLine(var);
                acum += var; /* acum+= var es igual a: acum= acum + var*/
            }
            Console.WriteLine("La suma del arreglo es = " + acum);
            Console.ReadKey();
        }
    }
}
