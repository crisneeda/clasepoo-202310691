﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog3
{
    class Promedio
    {
        private int Cal1;
        private int Cal2;
        private int Cal3;
        private double promedio;

        public Promedio(int x1,int x2,int x3)
        {
            Cal1 = x1;
            Cal2 = x2;
            Cal3 = x3;
        }
        private void CalcPromedio(double prm)
        {
            prm = (Cal1 + Cal2 + Cal3) / 3;
            this.promedio=prm;
        }

        public double MostrarPromedio()
        {
            int Prom = 0;
            CalcPromedio(Prom);
            return this.promedio;
        }
    }
}
