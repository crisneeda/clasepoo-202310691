﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingresa la calificación 1.");
            int c1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa la calificación 2.");
            int c2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa la calificación 3.");
            int c3 = int.Parse(Console.ReadLine());
            Promedio objPromedio = new Promedio(c1,c2,c3);

            double MProm=objPromedio.MostrarPromedio();
            Console.WriteLine("El promedio es = " + MProm);
            Console.ReadKey();
        }
    }
}
