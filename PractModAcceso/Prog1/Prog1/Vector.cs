﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog1
{
    class Vector
    {
        //Creacion de un arreglo unidimensional de 20 espacios int, en private
        private static int[] a = new int[20];

        // Creacion metodo
        private void LeeDatos()
        {
            for (int i = 0; i < a.Length; i++)
            {
                //Uso de la propiedad "a", para rellenarse
                Console.WriteLine("Ingresa un valor");
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

        }

        public void MostrarDatos()
        {
            //Iniciacion del metodo LeeDatos dentro del metodo MostrarDatos
            LeeDatos();

            for (int i = 0; i < a.Length; i++)
            {
                //Escribe el arreglo a
                Console.WriteLine("El valor " + (i + 1) + " es igual a = " + a[i]);
            }
        }
    }
}
