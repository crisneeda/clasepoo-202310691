﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // creando una lista local en memoria
            int[] lista = new int[5];
            // cargando la lista local con 10,11,12,13,14
            for (int x = 0; x <= 4; x++)
            {
                lista[x] = x + 10;
            }

            proc1(lista);
            // Pasandola a procedimiento observar que va sin corhetes
            //desplegando lista original
            // observar corrida y ver si se desplego la
            // la lista original o la procesada dentro
            // del procedimiento
            for (int x = 0; x <= 4; x++)
            {
                listBox1.Items.Add(lista[x].ToString());
            }

        }

        void proc1(int[] vector) // se recibio con otro nombre y se creo sin tamano fijo
        {
            // procesando la lista recibida sumandole 15
            for (int x = 0; x <= 4; x++)
            {
                vector[x] = vector[x] + 15;
            }
            // observar que no se regresa la lista o vector recibido
        }
    }
}
