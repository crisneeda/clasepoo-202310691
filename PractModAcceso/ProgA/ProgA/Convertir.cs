﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgA
{
    class Convertir
    {
        //Creacion de tres parametros (int,double,double) en private
        private int Pesos = 800;
        private double dolar = 19.90;
        private double Conversion;

        //Inicio del metodo Convesor de tipo void, con un parametro, y en private 
        private void Conversor(double y)
        {
            //Operacion -> parametro= atributo
            y = Conversion;
            //Cambio de valor de Conversion
            Conversion = Pesos / dolar;
        }

        //Inicio del metodo Mostrar de tipo double, con un parametro, y en public
        public double Mostrar(double x)
        {
            //Inicializacion del metodo "Conversor"
            Conversor(x);
            x = Conversion;
            //retorno del parametro
            return x;
        }
    }
}
