﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgA
{
    class Program
    {
        static void Main(string[] args)
        {
            //intanciacion de la clase "Convertir"
            Convertir objConvertir = new Convertir();
            //Creacionde una variable que sera usada como parametro
            double x=0 ;
            //Asignacion de valor a una variable con "objConvertir.Mostrar"
            double Res = objConvertir.Mostrar(x);
            //Escritura de un mensaje y la variable Res
            Console.WriteLine("800.00 Pesos MXN = " + Res + " Dolares USD");

            Console.ReadKey();
        }
    }
}
