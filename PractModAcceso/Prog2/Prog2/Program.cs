﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creacion y asignacion de valores de 2 variables, que seran usadas como parametros
            Console.WriteLine("Ingresa el valor de n de la matriz");
            int Main_n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingresa el valor de m de la matriz");
            int Main_m = Convert.ToInt32(Console.ReadLine());
            //Instanciacion de la clase Matriz
            Matriz objMatriz = new Matriz();
            //Inicializacion de el metodo MostrarDatos del objeto objMatriz
            objMatriz.MostrarDatos(Main_n, Main_m);

            Console.ReadKey();

        }
    }
}
