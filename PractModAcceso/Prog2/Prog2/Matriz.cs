﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog2
{
    class Matriz
    {
        //Creacion de 2 variables y un arreglo bidimencionalm, usadas como propiedades
        private int n;
        private int m;
        private int[,] a;

        //inicio del metodo LeeDatos que tiene 2 parametros
        private void LeeDatos(int ValN, int ValM)
        {
            //Asignacion de valores a las propiedades n,m y tamaño del arreglo a
            n = ValN;
            m = ValM;
            a = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    //LLenado del arreglo a
                    Console.WriteLine("Ingresa un valor");
                    a[i,j] = Convert.ToInt32(Console.ReadLine());
                }
            }

        }

        //inicio del metodo Mostrar que tiene 2 parametros
        public void MostrarDatos(int SetN, int SetM)
        {
            //Inicializacion del metodo LeeDatos
            LeeDatos(SetN, SetM);

            for (int i = 0; i < SetN; i++)
            {
                for (int j = 0; j < SetM; j++)
                {
                    //Impresion del arreglo a
                    Console.WriteLine("El valor [" + (i + 1) + "," + (j + 1) + "] es igual a = " + a[i, j]);
                }
            }
        }
    }
}
