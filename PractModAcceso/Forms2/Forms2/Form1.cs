﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Metodo button1_click, el cual ejecuta el codigo cuando se da click al boton
        private void button1_Click(object sender, EventArgs e)
        {
            //Creacion de un arreglo unidimensional int de tamaño 10
            int[] a = new int[10];
            for (int x = 0; x < 10; x++)
            {
                //multiplica cada elemento del arreglo por 6 y lo guarda en un listbox
                a[x] = x * 6;
                listBox1.Items.Add(a[x].ToString());
                //LLama al metodo Calcula y guarda los resultados en otro listbox
                Calcula(a);
                listBox2.Items.Add(a[x].ToString());
            }
        }

        //Creacion de un metodo con un parametro en forma de arreglo
        void Calcula(int[] b)
        {
            int x;
            for (x = 0; x < 10; x++)
            {
                //Multiplica cada elemento del arreglo por 12
                b[x] = b[x] * 12;
            }
        }
    }
    
}
