﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgC
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creacion y asignacion de valor a una variable que sera usada como parametro
            Console.WriteLine("Ingresa la cantidad de calificaciones");
            int n = Convert.ToInt32(Console.ReadLine());

            //Insatanciacion de la clase Calificaciones 
            Calificaciones objCal = new Calificaciones(n);
            //Instanciacion del objeto "objCal" y de su metodo "Mostrar"
            objCal.Mostrar();

            Console.ReadKey();
        }
    }
}
