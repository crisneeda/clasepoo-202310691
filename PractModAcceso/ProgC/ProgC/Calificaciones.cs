﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgC
{
    class Calificaciones
    {
        //Atributos (arreglo private de tipo double y variable private de tipo int)
        private double[] Calif;
        private int n;

        //Constructor con un parametro que asigna valor a el atributo n y le asigan tamaño a el arreglo Calif
        public Calificaciones(int setN)
        {
            n = setN;
            Calif = new double[n];
        }

        //Metodo (privado de tipo void) que de asigna valores ingresados a el arreglo por medio de un for 
        private void Guardar()
        {
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Ingresa un valor para la CALIFICACIÓN " + (i + 1));
                Calif[i] = Convert.ToDouble(Console.ReadLine());
            }
        }

        //Metodo (prublic de tipo void) que inicia al metodo Mostrar Y escribe un mesaje con los valores de el arreglo Calif con un for
        public void Mostrar()
        {
            Guardar();
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("La calificación " + (i + 1) + " es: " + Calif[i]);
            }
        }
    }
}
