﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forms3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //Metodo button1_click que ejecuta el codigo dentro cuando se da click al boton
        private void button1_Click(object sender, EventArgs e)
        {
            //Llamada al metodo "metodo"
            metodo();
        }

        //Creacion del metodo "metodo" de tipo void
        void metodo()
        {
            //Creacion y asigancion de valor(Con un textbox)  a la variable edad  
            int edad = Int32.Parse(EDAD.Text);
            //Murtiplicaion de la variable edad´por 12
            edad = edad * 12;
            //Asignacion de valor a la label con el valor de edad
            MESES.Text = edad.ToString();
        }
    }
}
