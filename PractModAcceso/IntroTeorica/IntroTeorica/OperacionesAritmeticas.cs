﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroTeorica
{
    class OperacionesAritmeticas
    {
        /*Se crean varios metodos en public, que realizan distintas operaciones con los parametros a y b,
         y al final retornan el resultado*/
        public double suma(double a, double b)
        {
            return a + b;
        }
        public double resta(double a, double b)
        {
            return a - b;
        }
        public double multiplica(double a, double b)
        {
            return a * b;
        }
        public double Divide(double a, double b)
        {
            return a / b;
        }
    }
}
