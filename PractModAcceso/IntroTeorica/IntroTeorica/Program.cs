﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntroTeorica
{
    class Program
    {
        static void Main(string[] args)
        {
            // Se crean 2 variables para pasarlas como parametros
            int a, b;
            //Instanciacion de la clase "OperacionesAritmeticas"
            OperacionesAritmeticas oper = new OperacionesAritmeticas();
            //Se pide introducir valores para las variables
            Console.WriteLine("Teclea 2 numeros");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            //Se instancian los diferentes metodos del objeto "oper"
            Console.WriteLine(oper.suma(a, b));
            Console.WriteLine(oper.resta(a, b));
            Console.WriteLine(oper.multiplica(a, b));
            Console.WriteLine(oper.Divide(a, b));
            Console.ReadKey();
        }
    }
}
