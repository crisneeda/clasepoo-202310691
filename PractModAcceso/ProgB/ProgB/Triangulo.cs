﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgB
{
    class Triangulo
    {
        //Atributos privados de tipo float
        private float Base;
        private float Altura;
        private float Area;

        //Costructor que asigna valor a los atributos Base y Altura
        public Triangulo(float CBase,float CAltura)
        {
            Base = CBase;
            Altura = CAltura;
        }

        //Metodo (private de tipo void) que guarda un valor en el atributo Area 
        private void Calcular()
        {
            this.Area = (Base * Altura) / 2;
        }

        //Metodo (private de tipo float) que inica al metodo calcular y retona el valor del atributo Area
        public float Mostrar()
        {
            Calcular();
            return this.Area;
        }
    }
}
