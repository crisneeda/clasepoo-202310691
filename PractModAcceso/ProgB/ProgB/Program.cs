﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgB
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creacion y asignacion de valores a 2 variables (float), para usarlas de parametros
            Console.WriteLine("Ingresa el valor de la BASE del triangulo");
            float b = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa el valor de la ALTURA del triangulo");
            float a = float.Parse(Console.ReadLine());
            //Instanciacion de la clase "Triangulo", con los paramtros a y b
            Triangulo objTriangulo = new Triangulo(b, a);
            //Creacion y asignacion de valore a una variable con el valor del metodo del objeto "objTriangulo.Mostrar"
            float Ar = objTriangulo.Mostrar();
            //Escritura de un mensaje y la variable Ar
            Console.WriteLine("El area del trinagulo es: " + Ar);

            Console.ReadKey();
        }
    }
}
