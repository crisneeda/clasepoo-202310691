﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se crean dos arreglos int y float de 4 espacios
            int[] a = new int[4];
            float[] b = new float[4];
            //Instanciación de la clase OrdVar
            OrdVar p = new OrdVar();
            //Se llenan los arreglos con valores que da el usuario
            Console.WriteLine("Elementos del arreglo:");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Elementos del arreglo:");
            for (int j = 0; j < b.Length; j++)
            {
                b[j] = Convert.ToInt32(Console.ReadLine());
            }
            //Se inician los metodos del objeto b
            p.Ordenar(a);
            p.Ordenar(b);
            Console.ReadLine();
        }
        
    }
}
