﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo
{
    class OrdVar
    {
        //Se crean 2 metodos con "paramentros arreglo"
        public void Ordenar(params int[] a)
        {
            Console.WriteLine("-------------ORDENA-----------");
            // Se utiliza la libreria sort que ordena de manera ascendente los valores de un arreglo
            Array.Sort(a);
            //Se imprime el arreglo con un foreach
            foreach (int i in a)
                Console.WriteLine(+i);
        }
        public void Ordenar(params float[] b)
        {
            //reordena
            Console.WriteLine("-------------REORDENA-----------");
            // Se utiliza la libreria reverse que ordena un arreglo al "reves"
            Array.Reverse(b);
            foreach (int i in b)
                Console.WriteLine(+i);
        }
    }
}
