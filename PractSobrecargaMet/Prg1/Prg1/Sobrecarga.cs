﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Sobrecarga
    {
        //Metodo sobrecargado con 2 parametros
        public void Grande(int x1, int x2)
        {

            Console.WriteLine("Los valores de este metodo sobrecargado 1 son: "+ x1+ " y "+ x2);
        }

        //Metodo sobrecargado con 3 parametros
        public void Grande(int x1, int x2, int x3)
        {
            Console.WriteLine("Los valores de este metodo sobrecargado 2 son: " + x1+ " , "+ x2+ " y "+ x3);
        }

        //Metodo sobrecargado con 4 parametros
        public void Grande(int x1, int x2, int x3,int x4)
        {
            Console.WriteLine("Los valores de este sobrecargado 3 son: " + x1+ " , "+ x2+ " , "+ x3+ " y "+ x4);
        }
    }
}
