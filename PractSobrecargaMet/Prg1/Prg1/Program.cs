﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciación de la clase Sobrecarga
            Sobrecarga objSbrCar = new Sobrecarga();

            /*Inicializacion de los metodos sobrecargados "Grande"
             diferenciandolos por numero de parametros que necesitan */
            objSbrCar.Grande(10, 4);
            objSbrCar.Grande(15, 35, 23);
            objSbrCar.Grande(10, 12, 12, 18);

            Console.ReadKey();
        }
    }
}
