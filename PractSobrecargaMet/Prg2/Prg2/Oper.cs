﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2
{
    class Oper
    {
        //Se crean 3 metodos sobrecargados con diferente tipo de valor
        public int Cuadrado(int n)
        {
            int Cuad = n * n;
            return Cuad;
        }

        public double Cuadrado(double n)
        {
            double Cuad = Math.Pow(n, 2);
            return Cuad;
        }

        public float Cuadrado(float n)
        {
            float Cuad = n*n;
            return Cuad;
        }
    }
}
