﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se piden 3 valores al usuario
            Console.WriteLine("Ingresa un numero entero");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingresa un numero real");
            double y = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ingresa un numero flotante");
            float z = float.Parse(Console.ReadLine());

            //Instanciación de la clase Oper
            Oper objOper = new Oper();
            //Instanciacion de los metodos sobrecargados del objOper diferenciandolos por el tipo de valor del parametro.
            Console.WriteLine("El resultado entero es: " + objOper.Cuadrado(x));
            Console.WriteLine("El resultado real es: " + objOper.Cuadrado(y));
            Console.WriteLine("El resultado flotante es: " + objOper.Cuadrado(z));

            Console.ReadKey();
        }
    }
}
