﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables que llena el usuario
            Console.WriteLine("Ingresa un valor negativo");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingresa un valor positivo");
            int p = Convert.ToInt32(Console.ReadLine());
            //Instanciacion de la clase Vector
            Vector objVector = new Vector(n,p);
            //Instanciacipn del metodo Imprimir del objeto objVector
            objVector.Imprimir();

            Console.ReadKey();
        }
    }
}
