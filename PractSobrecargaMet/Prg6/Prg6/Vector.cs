﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg6
{
    class Vector
    {
        //Atributos; arreglo int de 300 espacios, int a y b
        private int[] v=new int[300];
        private int a;
        private int b;

        //Constuctor para darle valores a los atributos
        public Vector(int n, int p)
        {
            a = n;
            b = p;
            /*LLenado del arreglo con un for y la opcion random El unico problema que encontre
              que que se generaban varios numero con el mismo valor, porque se compila demasiado rapido*/
            for (int i = 0; i < v.Length; i++)
            {
                Random x = new Random();
                v[i] = x.Next(a,b);
                Console.WriteLine("["+i+"]: "+v[i]);

            }
        }

        //Metodo de tipo arreglo int con un parametro arreglo int
        private int []Calcular(int []array)
        {
            //Arreglo que sirve para contabilizar y al final retornarlo
            int[] con = new int[3];
            for (int i = 0; i < v.Length; i++)
            {
                if(v[i]==0)
                {
                    con[0]++;
                }
                else
                {
                    if(v[i]>0)
                    {
                        con[1]++;
                    }
                    else
                    {
                        con[2]++;
                    }
                }
            }

            return con;
        }
        
        //Arreglo que istancia al metodo calcular, almacena el retorno y al final imprime
        public void Imprimir()
        {
            int[] mostrar = Calcular(v);

            Console.WriteLine("La cantidad de valores iguales a 0 es: " + mostrar[0]);
            Console.WriteLine("La cantidad de valores POSITIVOS es: " + mostrar[1]);
            Console.WriteLine("La cantidad de valores NEGATIVOS es: " + mostrar[2]);
        }
    }
}
