﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg5
{
    class Arrays
    {
        //Metodos sobrecargado de tipo arreglo short y long,
        //que piden un parametro arreglo del mismo tipo que ellos
        private short[] Array(short[] a)
        {
            //Decalaracion de arreglo de 2 espacios para despues retornarlo
            short []x =new short[2];
            for (short i=0; i < a.Length; i++) 
            {
                if (a[i] > x[0])
                {
                    x[0] = a[i];
                    x[1] = i;
                }
            }

            return x;
        }

        private long[] Array(long[] a)
        {
            //Decalaracion de arreglo de 2 espacios para despues retornarlo
            long[] l = new long[2];
            for (long i = 0; i < a.Length; i++)
            {
                if(a[i]>l[0])
                {
                    l[0] = a[i];
                    l[1] = i;
                }
            }

            return l;
        }

        //Metodo que instancia e imprime los metodos sobrecargados
        public void Imprimir(short[] s, long[] l)
        {
            //Decalaracion de arreglos para almacenar el retorno de los metodos sobrecargados
            short[] ArSh = Array(s);
            long[] ArLg = Array(l);
            Console.WriteLine("El valor del elemento mayor del SHORT es: [" + ArSh[1] + "]: " + ArSh[0]);
            Console.WriteLine("El valor del elemento mayor del LONG es: [" + ArLg[1] + "]: " + ArLg[0]);
        }
    }
}
