﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciacion de la clase Arrays
            Arrays objArrays = new Arrays();

            //Cración y llenado de arreglos (por parte del usuario)
            Console.WriteLine("Ingresa valores para el arreglo short");
            short[] arsh = new short[5];
            for (int i = 0; i < arsh.Length; i++)
            {
                Console.Write("Valor " +"["+(i)+"]: ");
                arsh[i] = short.Parse(Console.ReadLine());
            }

            Console.WriteLine("Ingresa valores para el arreglo long");
            long[] arlg = new long[5];
            for (int i = 0; i < arlg.Length; i++)
            {
                Console.Write("Valor " + "[" + (i) + "]: ");
                arlg[i] = long.Parse(Console.ReadLine());
            }

            //Instanciacion del metos Imprimir del objeto objArrays
            objArrays.Imprimir(arsh,arlg);

            Console.ReadKey();
        }
    }
}
