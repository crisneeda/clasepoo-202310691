﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pgr3
{
    class Lista
    {
        //Se crean 3 atributos; 2 arreglos string e int, y 1 atributo int=35
        private string[] Nom;
        private int[] Tel;
        private int Tm=35;

        /*Constructor que sirve para cambiar el valor predeterminado del atributo Tm
         y así definir un tamaño para los atributos arreglos*/
        public Lista(string res)
        {
            switch (res)
            {
                case "si":
                    Console.WriteLine("Ingresa el tamaño");
                    Tm = Convert.ToInt32(Console.ReadLine());
                    Nom = new string[this.Tm];
                    Tel = new int[this.Tm];
                    break;

                default:
                    Nom = new string[Tm];
                    Tel = new int[Tm];
                    break;
            }
        }

        //Metodo que le asigna valores a los atributos arreglos
        private void Insertar()
        {
            for (int i = 0; i < Tm; i++)
            {
                Console.WriteLine("Ingresa NOMBRE "+(i+1));
                string n = Convert.ToString(Console.ReadLine());
                Console.WriteLine("Ingresa TELEFONO "+(i+1));
                int t = Convert.ToInt32(Console.ReadLine());

                Nom[i] = n;
                Tel[i] = t;
            }
        }

        //Metodo que imprime los arreglos
        public void Mostrar()
        {
            //Inicialización del metodo Insertar
            Insertar();
            Console.WriteLine("");
            Console.WriteLine("Los datos son: ");
            for (int i = 0; i < Tm; i++)
            {
                Console.WriteLine(Nom[i] + " : " + Tel[i]);
            }
        }
    }
}
