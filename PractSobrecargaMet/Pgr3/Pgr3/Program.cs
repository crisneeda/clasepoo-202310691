﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pgr3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Pregunta que sirve para darle un valor al parametro del constructor de Lista
            Console.WriteLine("¿Deseas cambiar ingresar el tamaño del arreglo?  Si[si]  No[Cualquier valor]");
            string res = Console.ReadLine();
            
            //Intanciación de la clase Lista
            Lista objlst = new Lista(res);
            //Instanciación de metodo mostrar del objeto objlst
            objlst.Mostrar();

            Console.ReadKey();
        }
    }
}
