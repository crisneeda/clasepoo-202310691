﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg4
{
    class Valor
    {
        private int n;
        //Constructor para dar valor a n
        public Valor(int n)
        {
            this.n = n;
        }

        //Metodos Ordenar sobrecagados con arreglos de distinto tipo
        private void Ordenar(int []edad)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (edad[j] > edad[j + 1])
                    {
                        int T = edad[j];
                        edad[j] = edad[j + 1];
                        edad[j + 1] = T;
                    }
                }
            }

            Console.WriteLine("El arreglo int ordenado es: ");
            for (int i = 0; i < edad.Length; i++)
            {
                Console.Write(edad[i]+"  ");
            }
        }

        private void Ordenar(double []edad)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n - 1; j++)
                {
                    if (edad[j] > edad[j + 1])
                    {
                        double T = edad[j];
                        edad[j] = edad[j + 1];
                        edad[j + 1] = T;
                    }
                }
            }

            Console.WriteLine("El arreglo double ordenado es: ");
            for (int i = 0; i < edad.Length; i++)
            {
                Console.Write(edad[i]+"  ");
            }
        }

        //metodos que inicia los metodos Ordenar sobrecargados
        public void Mostrar(int[] IntEd, double[] DoubleEd)
        {
            Ordenar(IntEd);
            Console.WriteLine("");
            Ordenar(DoubleEd);
        }
    }
}
