﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prg4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Valor que da el usuario, el cual asigna tamaño a 2 arreglos
            Console.WriteLine("Ingresa el tamaño de los arreglos");
            int a = Convert.ToInt32(Console.ReadLine());
            int[] IntEd = new int[a];
            double[] DoubleEd = new double[a];

            //LLenado de los arreglos
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("Ingresa un valor entero");
                IntEd[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < a; i++)
            {
                Console.WriteLine("Ingresa un valor double");
                DoubleEd[i] = Convert.ToDouble(Console.ReadLine());
            }

            //Intanciación de la clase Valor
            Valor objVal = new Valor(a);
            //Instanción de metodo Mostrar del objeto objVal, el cual da los  arreglos como parametros
            objVal.Mostrar(IntEd, DoubleEd);

            Console.ReadKey();
        }
    }
}
