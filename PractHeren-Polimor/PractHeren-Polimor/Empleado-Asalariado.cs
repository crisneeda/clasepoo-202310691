﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractHeren_Polimor
{
    //Clase que hereda de la clase abstracta "Empleado"
    class Empleado_Asalariado:Empleado
    {
        //Atributo privado para la clase
        private double salarioSemanal;
        //Sobreescritura del metodo abstracto de la clase base
        public override void CalcularSalario()
        {
            Console.WriteLine("Ingresa nombre");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Ingresa apellido");
            apellidoPaterno = Console.ReadLine();
            Console.WriteLine("Ingresa NSS");
            NSS = Convert.ToInt32(Console.ReadLine());
            salarioSemanal = 3500;
            Console.WriteLine(primerNombre + "\n" + apellidoPaterno + "\n" + NSS + "\n" + salarioSemanal);
        }
    }
}
