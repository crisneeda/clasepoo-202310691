﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractHeren_Polimor
{
    class Program
    {
        //Main que al inicio instancia todas las clases con las que se van a trabajar
        static void Main(string[] args)
        {
            Empleado_Asalariado objAsalariado = new Empleado_Asalariado();
            EmpleadoPorHoras objEmpHoras = new EmpleadoPorHoras();
            EmpleadoPorComision objEmpCom = new EmpleadoPorComision();
            EmpleadoBaseMasComision objEmpBaseCom = new EmpleadoBaseMasComision();
            //Instruccion para que el usuario inserte un valor para escoger que calculo hacer
            Console.WriteLine("Ingresa de quien quieres obtener el sueldo");
            Console.WriteLine(" Asalariado [1]\n Empleado por horas [2]\n Empleado por comisión[3]\n Empleado base más comisión[4]");
            string n = Console.ReadLine();

            switch(n) //Switch que ejecutara un objeto dependiendo del numero, y el default regresa un mensaje
            {
                case "1":
                    objAsalariado.CalcularSalario();
                    break;
                case "2":
                    objEmpHoras.CalcularSalario();
                    break;
                case "3":
                    objEmpCom.ImprimirSalario();
                    break;
                case "4":
                    objEmpBaseCom.Sueldo();
                    break;
                default:
                    Console.WriteLine("Valor no válido");
                    break;
            }

            Console.ReadLine();
        }
    }
}
