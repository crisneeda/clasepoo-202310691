﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractHeren_Polimor
{
    //Clase abstracta que hereda tres atributos, y un metodo
    abstract class Empleado
    {
        protected string primerNombre;
        protected string apellidoPaterno;
        protected int NSS;

        public abstract void CalcularSalario();
    }
    //Clase normal que herdea tres atributos, y un metodo virtual
    class EmpleadoVirtual
    {
        protected string primerNombre;
        protected string apellidoPaterno;
        protected int NSS;

        public virtual void CalcularSalario()
        {
            Console.WriteLine(primerNombre + "\n" + apellidoPaterno + "\n" + NSS);
        }
    }
    //Interfaz con un metodo
    interface EmpleadoInterface
    {
        double CalcularSalario();
    }
}
