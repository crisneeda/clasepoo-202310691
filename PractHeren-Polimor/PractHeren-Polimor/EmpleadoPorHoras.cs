﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractHeren_Polimor
{
    //Clase que hereda de la clase "EmpleadoVirtual" 
    class EmpleadoPorHoras:EmpleadoVirtual
    {
        //Atributos privados de la clase
        private double sueldo=125;
        private int horas;
        //Sobre escritura del metodo virtual heredado
        public override void CalcularSalario()
        {
            //Datos a rellenar por el usuario
            Console.WriteLine("Ingresa nombre");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Ingresa apellido");
            apellidoPaterno = Console.ReadLine();
            Console.WriteLine("Ingresa NSS");
            NSS = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingresa horas trabajadas: ");
            horas = Convert.ToInt32(Console.ReadLine());
            //If para decidir que operacion hacer
            if (horas <= 40)
            {
                sueldo = sueldo * horas;
            }
            else
            {
                sueldo = 40 * sueldo + (horas - 40) * sueldo * 1.5;
            }
            //Ejecucion del metodo virtual original
            base.CalcularSalario();
            Console.WriteLine("Sueldo: " + sueldo);
        }
    }
}
