﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractHeren_Polimor
{
    //Clase que hereda de la clase EmpleadoPorComision, que a su vez herda de una interfaz
    class EmpleadoBaseMasComision:EmpleadoPorComision
    {
        //Atributo private de la clase
        private double salarioBase=3890;
        //Atributo independiente de la herencia, o sea, de la clase EmpleadoBaseMasComision
        public void Sueldo()
        {
            double sueldo=CalcularSalario();
            sueldo = sueldo * salarioBase;

            ImprimirSalario();
        }
    }
}
