﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PractHeren_Polimor
{
    //Clase que hereda de la interfaz EmpleadoInterface
    class EmpleadoPorComision: EmpleadoInterface
    {
        //Atributos protected, para poder heredar a otra clase
        protected string primerNombre;
        protected string apellidoPaterno;
        protected int NSS;
        protected int ventasBrutas;
        protected double tarifaComision=178;
        //Metodo de la Interfaz
        public double CalcularSalario()
        {
            Console.WriteLine("Ingresa nombre");
            primerNombre = Console.ReadLine();
            Console.WriteLine("Ingresa apellido");
            apellidoPaterno = Console.ReadLine();
            Console.WriteLine("Ingresa NSS");
            NSS = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingresa numero de ventas brutas: ");
            ventasBrutas = Convert.ToInt32(Console.ReadLine());

            double salario = tarifaComision * ventasBrutas;
            return salario;
        }
        //Metodo independiente de la interfaz, es decir, propio de la clase EmpleadoPorComision
        public void ImprimirSalario()
        {
            double salario = CalcularSalario();
            Console.WriteLine(primerNombre + "\n" + apellidoPaterno + "\n" + NSS);
            Console.WriteLine("Sueldo: " + salario);
        }

    }
}
