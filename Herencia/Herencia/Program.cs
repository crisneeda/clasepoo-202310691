﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Instanciación del objeto "objHijo" a partir de la clase base "Hijo", el cual hereda de la clase "Herencia".
            Hijo objHijo = new Hijo();
            //Uso del "metodo1", a partir del "objHijo" heredado de la clase "Herencia".
            objHijo.metodo1();
            //Uso del "metodoAcceso", a partir del "objHijo".
            objHijo.metodoAcceso();

            //Instanciación del objeto "objHerencia" a partir de la clase base "Herencia"
            Herencia objHerencia = new Herencia();
            //Uso del "AccesoMetodo2", a partir del "objHerencia".
            objHerencia.AccesoMetodo2();
            Console.ReadKey();
        }
    }
}
