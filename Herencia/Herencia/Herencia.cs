﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Herencia
    {
        private int atributo1; //Atributo private (solo puede ser usado por la clase).
        public int atributo2; //Atributo public (permite cualquier acceso a él).
        protected int atributo3; //Atributo protected (solo puede ser usado por la clase y sus herencias).

        // Metodo public (permite cualquier acceso a él).
        public void metodo1()
        {
            Console.WriteLine("Metodo 1 de la clase Herencia");
        }

        //Atributo private (solo puede ser usado por la clase).
        private void metodo2()
        {
            Console.WriteLine("Este metodo PRIVATE no se puede acceder desde fuera de la clase Herencia");
        }

        //Metodo protected (solo puede ser usado por la clase y sus herencias).
        protected void metodo3()
        {
            Console.WriteLine("Metodo 3 de la clase Herencia");
        }

        //Metodo que encapsula en metodo2 (private), para poder acceder a este desde cualquier clase.
        public void AccesoMetodo2()
        {
            metodo2();
        }

    }

    //Clase "Hijo", que hereda de la clase "Herencia".
    class Hijo: Herencia
    {
        //Metodo public que accede a el "metodo3" que es heredado de la clase "Herencia".
        public void metodoAcceso()
        {
            metodo3();
        }
    }
}
