﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaseAbstracta
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto objAuto = new Auto();
            Avion objAvion = new Avion();

            objAuto.Mantenimiento();
            objAvion.Mantenimiento();

            Console.ReadKey();
        }
    }
}
