﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaseAbstracta
{
    abstract class Transporte
    {
        public abstract void Mantenimiento();
    }

    class Auto : Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Mantenimiento Auto\n Cambio de aceite\n Cambio bujias\n Chequeo llantas\n Liquido de frenos");
        }
    }

    class Avion:Transporte
    {
        public override void Mantenimiento()
        {
            Console.WriteLine("Mantenimiento Avion\n Chequeo motor\n Chequeo alas\n chequeo gasolina");
        }
    }
}
