﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SobrecargaMet
{
    class Program
    {
        static void Main(string[] args)
        {
            Operacion ObjOperacion = new Operacion();
            int Res=ObjOperacion.Suma(3, 4);
            Console.WriteLine("Resultado metodo 1" + Res);

            double Res2 = ObjOperacion.Suma(3.54, 4.546,3.3443);
            Console.WriteLine("Resultado metodo 2" + Res2);

            float Res3 = ObjOperacion.Suma(3.4f,41.1f,56.34f);
            Console.WriteLine("Resultado metodo 2" + Res2);

        }
    }
}
