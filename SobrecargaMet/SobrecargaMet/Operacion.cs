﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SobrecargaMet
{
    class Operacion
    {
        public int Suma (int a, int b)
        {
            int Resul = a + b;
            return Resul;
        }

        public double Suma(double a, double b, double c)
        {
            double Resul = a + b+c;
            return Resul;
        }

        public float Suma(float a, float b, float c)
        {
            float Resul = a + b + c;
            return Resul;
        }
    }
}
