﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU1
{
    class Perro
    {
        private int tamano = 0;
        private string raza = "";
        private string color = "";
        private int peso = 0;
        private int edad = 0;

        public void setTamano(int tam)
        {
            this.tamano = tam;
        }
        public int getTamano()
        {
            return this.tamano;
        }
        public void setRaza(string raz)
        {
            this.raza = raz;
        }
        public string getRaza()
        {
            return this.raza;
        }
        public void setColor(string col)
        {
            this.color = col;
        }
        public string getColor()
        {
            return this.color;
        }
        public void setPeso(int pes)
        {
            this.peso = pes;
        }
        public int getPeso()
        {
            return this.peso;
        }
        public void setEdad(int ed)
        {
            this.edad = ed;
        }
        public int getEdad()
        {
            return this.edad;
        }


    }
}
