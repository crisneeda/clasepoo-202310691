﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenU1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_Mostrar_Click(object sender, EventArgs e)
        {
            Perro ObjPerro = new Perro();

            ObjPerro.setColor("Cáfe");
            string VarColor = ObjPerro.getColor();
            MessageBox.Show("El color del perro 1 es: " + VarColor);

            ObjPerro.setTamano(10);
            int VarTam = ObjPerro.getTamano();
            MessageBox.Show("El tamaño del perro 1 es: " + VarTam);

            ObjPerro.setPeso(15);
            int VarPeso = ObjPerro.getPeso();
            MessageBox.Show("El peso del perro 1 es: " + VarPeso);

            ObjPerro.setRaza("Pitbull");
            string VarRaza = ObjPerro.getRaza();
            MessageBox.Show("El raza del perro 1 es: " + VarRaza);

            ObjPerro.setEdad(5);
            int VarEdad = ObjPerro.getEdad();
            MessageBox.Show("El peso del perro 1 es: " + VarEdad);
        }
    }
}
