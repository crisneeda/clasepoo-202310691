﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingresa un número");
            int numero = Convert.ToInt32(Console.ReadLine());

            Primo objPrimo = new Primo(numero);
            objPrimo.ImprimePrimo();

            Console.ReadKey();
        }
    }
}
