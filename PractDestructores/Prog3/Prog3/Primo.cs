﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog3
{
    class Primo
    {
        private int num;
        private int a;

        public Primo(int setNum)
        {
            this.num = setNum;

            for (int i = 1; i < (this.num + 1); i++)
            {
                if (this.num % i == 0)
                {
                    this.a++;
                }
            }
        }

        public void ImprimePrimo()
        {
            if (this.a == 2)
            {
                Console.Write(this.num+" es primo");
            }
            else
            {
                Console.WriteLine(this.num+" no es primo");
            }
        }

       ~Primo()
        {
            System.Diagnostics.Trace.WriteLine("El destructor esta funcionando");
        }
    }
}
