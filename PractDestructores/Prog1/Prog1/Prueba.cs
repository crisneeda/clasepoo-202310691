﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog1
{
    class Prueba
    {
        public Prueba(int x)
        {
            System.Console.Write("Creado objeto Prueba con x={0}", x);
            Console.ReadLine();
        }

        ~Prueba()
        {
            System.Diagnostics.Trace.WriteLine("El destructor funciona");
        }

    }
}
