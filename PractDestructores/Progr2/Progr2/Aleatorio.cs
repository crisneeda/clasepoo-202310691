﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Progr2
{
    class Aleatorio
    {
        private int atribRdm;
        public Aleatorio()
        {
            Random Rdm = new Random();
            this.atribRdm=Rdm.Next(0, 250);
        }
        
        public void ImprimirAleatorio()
        {
            Console.WriteLine("El número aleatorio es: "+ Convert.ToInt32(this.atribRdm));

        }

        ~Aleatorio()
        {
            System.Diagnostics.Trace.WriteLine("El destructor funciona");
        }
    }
}
