﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class VolumenCilindro:AreaCirculo
    {
        private double volumen;
        private double altura;
 
        public VolumenCilindro(double setDiam,double setAltura) : base (setDiam)
        {
            this.altura = setAltura;
            this.diametro = setDiam;
        }

        private void CalcularVolumen()
        {
            CalcularArea();
            this.volumen = this.area * this.altura;
        }

        public double MostrarVolumen()
        {
            CalcularVolumen();
            double getVolumen = this.volumen;
            return getVolumen;
        }

        ~VolumenCilindro()
        {
            System.Diagnostics.Trace.WriteLine("El destructor de la clase VolumenCilindro funciona");
        }


    }
}
