﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Calculadora de un cilindro a partir de un diametro de un circulo");

            Console.WriteLine("Ingresa un valor para el diametro");
            int diam = int.Parse(Console.ReadLine());
            AreaCirculo objArea = new AreaCirculo(diam);

            double area = objArea.MostrarArea();
            Console.WriteLine("El area del circulo es " + area + " Unidades ^ 2"); 

            Console.WriteLine("Ingresa un valor para el altura del cilindro");
            double altu = int.Parse(Console.ReadLine());
            VolumenCilindro objVol = new VolumenCilindro(diam, altu);

            double vol = objVol.MostrarVolumen();
            Console.WriteLine("El volumen del cilindro es " + vol + " Unidades ^ 3");

            Console.ReadKey();
        }
    }
}
