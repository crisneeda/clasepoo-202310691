﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenU3
{
    class AreaCirculo
    {
        protected double diametro;
        protected double radio;
        protected double area;

        public AreaCirculo(double setDiam)
        {
            this.diametro = setDiam;
        }

        private void CalcularRadio()
        {
            double CRadio = diametro / 2;
            this.radio = CRadio;
        }

        protected void CalcularArea()
        {
            CalcularRadio();
            this.area = Math.PI * (this.radio * this.radio);
        }

        public double MostrarArea()
        {
            CalcularArea();
            double getArea = this.area;
            return getArea;
        }

        ~AreaCirculo()
        {
            System.Diagnostics.Trace.WriteLine("El destructor de la clase AreaCirculo funciona"); 
        }
    }
}
