﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConstructorForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int indice = cbxMultas.SelectedIndex;
            Operacion objOperacion = new Operacion(indice);
            lblRes.Text = Convert.ToString(objOperacion.CalcularMulta());

            MessageBox.Show("El índice seleccionado es: " + indice);
        }
    }
}
