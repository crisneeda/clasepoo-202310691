﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorForms
{
    class Operacion
    {
        private int multa;
        public Operacion(int setMulta)
        {
            this.multa = setMulta;
        }

        public int CalcularMulta()
        {
            int resultado = multa;
            switch(resultado)
            {
                case 0:
                    return resultado = 180;
                case 1:
                    return resultado = 2540;
                case 2:
                    return resultado = 2230;
                case 3:
                    return resultado = 300;
                default:
                    return resultado = 0;
            }
        }
        ~Operacion()
        {
            this.multa = 0;
            Console.WriteLine("Memoria vacía");
        }
    }
}
