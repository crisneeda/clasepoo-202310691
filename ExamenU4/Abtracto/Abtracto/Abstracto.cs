﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abtracto
{
    abstract class Abstracto
    {
        protected abstract float Cubo();
        protected abstract float Cuadrado();
        public abstract void Imprimir();
    }
}
