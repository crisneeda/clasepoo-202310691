﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abtracto
{
    class Hija:Abstracto
    {
        private float a;
        private float b;
        private float h;
        public Hija(float b,float h,float a)
        {
            this.b = b;
            this.h = h;
            this.a = a;
        }
        protected override float Cuadrado()
        {
            float area = b * h;
            return area;
        }

        protected override float Cubo()
        {
            float vol = b * h * a;
            return vol;
        }

        public override void Imprimir()
        {
            float area = Cuadrado();
            float vol = Cubo();

            Console.WriteLine("El area del cuadrado es: " + area);
            Console.WriteLine("El volumen del cubo es: " + vol);
        }
    }
}
