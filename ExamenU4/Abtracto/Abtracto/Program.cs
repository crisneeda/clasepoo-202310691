﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abtracto
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingresa la base");
            float b = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa la altura");
            float h = float.Parse(Console.ReadLine());
            Console.WriteLine("Ingresa la ancho");
            float a = float.Parse(Console.ReadLine());

            Hija objFormas = new Hija(b, h, a);
            objFormas.Imprimir();

            Console.ReadKey();
        }
    }
}
