﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaz
{
    class Hija:Interfaz
    {
        private float a;
        private float b;
        private float h;
        public Hija(float b, float h, float a)
        {
            this.b = b;
            this.h = h;
            this.a = a;
        }
        public float Cubo()
        {
            float vol = b * h * a;
            return vol;
        }

        public float Cuadrado()
        {
            float area = b * h;
            return area;
        }

    }
}
