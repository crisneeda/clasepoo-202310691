﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sobrecarga
{
    class Program
    {
        static void Main(string[] args)
        {
            Sobrecarga objSobre = new Sobrecarga();
            Console.WriteLine("Ingresa Nombre: ");
            string n = Console.ReadLine();
            Console.WriteLine("Ingresa Apellido Paterno: ");
            string ap = Console.ReadLine();
            Console.WriteLine("Ingresa Apellidio Materno: ");
            string am = Console.ReadLine();
            Console.WriteLine("Ingresa sus horas trabajadas");
            double h = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ingresa su sueldo por hora");
            double s = Convert.ToDouble(Console.ReadLine());

            string Nombre=objSobre.Empleado(n, ap, am);
            double pago=objSobre.Empleado(h, s);

            Console.WriteLine("El empleado " + Nombre + " recibe de sueldo: " + pago);
            Console.ReadLine();
        }
    }
}
