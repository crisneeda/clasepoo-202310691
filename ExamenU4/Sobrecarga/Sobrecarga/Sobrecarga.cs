﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sobrecarga
{
    class Sobrecarga
    {
        public string Empleado(string nombre,string apellP,string apellidoM)
        {
            string FullNom = apellP + " " + apellidoM + " " + nombre;
            return FullNom;
        }

        public double Empleado(double horas, double pago)
        {
            double Total = horas * pago;
            return Total;
        }
    }
}
